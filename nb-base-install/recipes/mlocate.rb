#
# Cookbook Name:: nb-base-install
# Recipe:: mlocate
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

Chef::Log.info("********** Running mlocate recipe. **********")

yum_package "mlocate" do
  package_name  "mlocate"
  action        :install
end

execute "Update the locate database" do
  command "updatedb"
end
